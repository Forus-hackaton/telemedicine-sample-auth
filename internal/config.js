module.exports = {
	server: {
		port: 7001
	},
	index: "./src/index.html",
	devEntry: {
		bundle: [
			"babel-polyfill",
			"./src/scripts/entry"
		],
		styles: [
			"./src/assets/fonts/icons.font",
			"./src/assets/fonts/nunito.plain.less"
		]
	},
	prodEntry: {
		bundle: [
			"babel-polyfill",
			"./src/scripts/entry",
			"./src/assets/fonts/icons.font",
			"./src/assets/fonts/nunito.plain.less"
		]
	}
};