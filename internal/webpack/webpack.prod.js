const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

const config = require("../config");

module.exports = {
	entry: config.prodEntry,
	output: {
		path: path.resolve(__dirname, "../../public"),
		publicPath: "/",
		filename: "[name]-[hash].js"
	},
	resolve: {
		modules: [
			path.resolve(__dirname, "../../src/scripts"),
			"node_modules",
			path.resolve(__dirname, "../../src/assets/images"),
			path.resolve(__dirname, "../../src/assets/video")
		],
		alias: {
			Constants$: path.resolve(__dirname, "../../config.json")
		},
		extensions: [".js", ".jsx"]
	},
	module: {
		loaders: [{
			test: /\.(js|jsx)?$/,
			loaders: ["babel-loader"],
			include: path.resolve(__dirname, "../../src/scripts") 
		}, {
			test: /\.json$/,
			loader: ["json-loader"]
		}, {
			test: /\.less$/,
			exclude: /\.plain.less$/,
			use: ExtractTextPlugin.extract({
				fallback: 'style-loader',
				use: [{
					loader: "css-loader", 
					options: {
						sourceMap: false,
						modules: true,
						camelCase: true,
						importLoaders: 2,
						localIdentName: "[name]__[local]"
					}
				}, {
					loader: "autoprefixer-loader",
					options: {
						browsers: ["last 10 versions", "ie 8"]
					}
				}, {
					loader: "less-loader"
				}],
			})
		}, {
			test: /\.plain\.less$/,
			use: ExtractTextPlugin.extract({
				fallback: 'style-loader',
				use: [{
					loader: "css-loader"
				}, {
					loader: "autoprefixer-loader",
					options: {
						browsers: ["last 10 versions", "ie 8"]
					}
				}, {
					loader: "less-loader"
				}],
			})
		}, {
			test: /\.font\.(js|json)$/,
			use: ExtractTextPlugin.extract({
				fallback: 'style-loader',
				use: [{
					loader: "css-loader"
				}, {
					loader: "fontgen-loader"
				}]
			})
		}, {
			test: /\.(png|gif|jpg)$/,
			use: [
				{
					loader: "url-loader",
					options: {
						name: "assets/images/[name].[ext]",
						limit: 1000
					}
				}
			]
		},{
			test: /\.(mp4|webm)$/,
			use: [
				{
					loader: "file-loader",
					options: {
						name: "assets/video/[name].[ext]"
					}
				}
			]
		}, {
			test: /\.(eot|ttf|woff|woff2|svg)$/,
			use: [{
				loader: "file-loader",
				options: {
					name: "assets/fonts/[name].[ext]"
				}
			}]
		}]
	},
	plugins: [
		new webpack.DefinePlugin({
			"process.env": {
				"NODE_ENV": JSON.stringify("production")
			}
		}),
		new CopyWebpackPlugin([{
			from: path.resolve(__dirname, "../../src/assets/images"),
			to: path.resolve(__dirname, "../../public/assets/images")
		}, {
			from: path.resolve(__dirname, "../../src/locales"),
			to: path.resolve(__dirname, "../../public/locales")
		}]),
		new ExtractTextPlugin("styles-[hash].css"),
		new HtmlWebpackPlugin({
			template: config.index,
			environment: "production",
			buildId: Date.now()
		}),
		new webpack.optimize.UglifyJsPlugin()
	]
};
