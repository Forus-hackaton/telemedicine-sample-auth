const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const config = require("../config");
const hotEntry = require("./hotEntry");

const entry = {};
for(var key in config.devEntry)
	entry[key] = hotEntry(...config.devEntry[key]);

module.exports = {
	entry: entry,
	output: {
		path: path.resolve(__dirname, "../../build"),
		publicPath: "/",
		filename: "[name].js"
	},
	resolve: {
		modules: [
			path.resolve(__dirname, "../../src/scripts"),
			"node_modules",
			path.resolve(__dirname, "../../src/assets/images"),
			path.resolve(__dirname, "../../src/assets/video")
		],
		alias: {
			Constants$: path.resolve(__dirname, "../../config.json"),
		},
		extensions: [".js", ".jsx"]
	},
	devtool: "cheap-module-source-map",
	module: {
		loaders: [{
			test: /\.(js|jsx)?$/,
			loaders: [
				"babel-loader",
				"eslint-loader"
			],
			include: path.resolve(__dirname, "../../src/scripts") 
		},{
			test: /\.json$/,
			loader: ["json-loader"]
		},{
			test: /\.less$/,
			exclude: /\.plain.less$/,
			use: [{
				loader: "style-loader"
			}, {
				loader: "css-loader", 
				options: {
					sourceMap: true,
					modules: true,
					camelCase: true,
					importLoaders: 2,
					localIdentName: "[name]__[local]"
				}
			}, {
				loader: "autoprefixer-loader",
				options: {
					browsers: ["last 10 versions", "ie 8"]
				}
			}, {
				loader: "less-loader", 
				options: {
					sourceMap: true,
					paths: [
						path.resolve(__dirname, "../../src/styles"),
						path.resolve(__dirname, "../../node_modules")
					]

				}
			}]
		},{
			test: /\.plain\.less$/,
			use: [{
				loader: "style-loader"
			}, {
				loader: "css-loader", 
				options: {
					sourceMap: true
				}
			}, {
				loader: "autoprefixer-loader",
				options: {
					browsers: ["last 10 versions", "ie 8"]
				}
			}, {
				loader: "less-loader", 
				options: {
					sourceMap: true
				}
			}]
		},{
			test: /\.font\.(js|json)$/,
			use: [{
				loader: "style-loader"
			}, {
				loader: "css-loader", 
				options: {
					sourceMap: true
				}
			}, {
				loader: "fontgen-loader", 
			}]
		},{
			test: /\.(png|gif|jpg)$/,
			use: [
				{
					loader: "url-loader",
					options: {
						name: "assets/images/[name].[ext]",
						limit: 1000
					}
				}
			]
		},{
			test: /\.(mp4|webm)$/,
			use: [
				{
					loader: "file-loader",
					options: {
						name: "assets/video/[name].[ext]"
					}
				}
			]
		},{
			test: /\.(eot|ttf|woff|woff2|svg)$/,
			use: [ {
				loader: "file-loader",
				options: {
					name: "assets/fonts/[name].[ext]"
				}
			}]
		}]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: config.index,
			environment: "development",
			buildId: Date.now()
		}),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin()
	]
};
