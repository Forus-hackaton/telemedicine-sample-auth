import Constants from "Constants";

export const constants = Constants;

export const apiUrl = constants.BASE_URL + "/api";

export const accessTokenStorageKey = "access_token";
export const accessTokenParam = "_accsstkn";

export const recaptchaSiteKey = null;
export const ravenConfigKey = null;

export const regex = {
	email: /^[a-zA-Z0-9!#$%&'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/
};

export const socialAccounts = {
	"twitter": null,
	"facebook": null,
	"instagram": null,
	"medium": null,
	"slack": null
};

export const languages = ["en"];
export const languageDefault = "en";