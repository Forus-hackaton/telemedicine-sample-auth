import { combineReducers, compose, createStore, applyMiddleware } from "redux";
import { routerReducer, routerMiddleware, syncHistoryWithStore } from "react-router-redux";
import { browserHistory } from "react-router";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";
import { multiClientMiddleware } from "redux-axios-middleware";

import {constants} from "config";
import clients from "clients";
import rootReducer from "reducer";

class Store {
	constructor(history){
		const initialState = {};

		const reducer = combineReducers({
			app: rootReducer,
			routing: routerReducer
		});
		const routingMiddleware = routerMiddleware(history);

		const middlewares = [routingMiddleware, thunk, multiClientMiddleware(clients)];

		if(!constants.SILENT) {
			middlewares.push(createLogger({
				collapsed: true
			}));
		}

		this.store = compose(
			applyMiddleware(...middlewares),
			window.devToolsExtension ? window.devToolsExtension() : f => f
		)(createStore)(reducer, initialState);
	}

	getStore(){
		return this.store;
	}
}

const store = new Store(browserHistory).getStore();

export default store;
export const history = syncHistoryWithStore(browserHistory, store);



