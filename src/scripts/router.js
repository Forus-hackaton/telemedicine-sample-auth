import React from "react";
import { Router, Route, IndexRoute } from "react-router";

import Root from "routes/Root/Root";
import ErrorPage from "routes/public/ErrorPage/ErrorPage";
import AuthSuccess from "routes/public/AuthSuccess/AuthSuccess";
import PublicHome from "routes/public/PublicHome/PublicHome";
import Cabinet from "routes/private/Cabinet/Cabinet";

const CustomRouter = (props) => {
	return (
		<Router history={ props.history }>
			<Route component={ Root }>
				<Route path={"/"} component={ PublicHome }/>
				<Route path={"/cabinet"} component={ Cabinet }/>
			</Route>
			<Route path={"/auth-success"} component={ AuthSuccess }/>
			<Route path={"*"} component={ ErrorPage }/>
			<Route path={"/404"} component={ ErrorPage }/>
		</Router>
	);
};

export default CustomRouter;