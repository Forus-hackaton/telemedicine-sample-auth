export const RESTORE_AUTH = "auth/RESTORE_AUTH";
export const SET_AUTH_TOKEN = "auth/SET_AUTH_TOKEN";

export const SET_PROFILE = "auth/SET_PROFILE";

export const SIGNOUT = "auth/SIGNOUT";

export const RESET = "auth/RESET";