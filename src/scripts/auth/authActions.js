import * as ActionTypes from "./authActionTypes";

export const restoreAuth = () => ({
	type: ActionTypes.RESTORE_AUTH
});

export const setProfile = data => ({
	type: ActionTypes.SET_PROFILE,
	data
});

export const setAuthToken = accessToken => ({
	type: ActionTypes.SET_AUTH_TOKEN,
	accessToken
});

export const signout = () => ({
	type: ActionTypes.SIGNOUT
});

export const resetAuthState = () => ({
	type: ActionTypes.RESET
});