import storage from "local-storage-fallback";

import * as Config from "config";
import { createReducer } from "utils/helpers";
import * as ActionTypes from "./authActionTypes";

const initialState = {
	isAuthorized: false,
	profile: null
};
const accessTokenStorageKey = Config.accessTokenStorageKey;
const profileStorageKey = "profileData";
const handlers = {};

handlers[ActionTypes.RESTORE_AUTH] = (state, action) => {
	const accessToken = storage.getItem(accessTokenStorageKey);
	const profile = storage.getItem(profileStorageKey);

	const isAuthorized = profile !== null ? true : false; // accessToken !== null ? true : false

	return Object.assign({}, state, {
		isAuthorized,
		profile: profile !== null ? JSON.parse(profile) : null
	});
};

handlers[ActionTypes.SET_PROFILE] = (state, action) => {
	const profile = action.data;
	storage.setItem(profileStorageKey, JSON.stringify(profile));
	return Object.assign({}, state, {
		isAuthorized: true,
		profile
	});
};

handlers[ActionTypes.SET_AUTH_TOKEN] = (state, action) => {
	const accessToken = action.accessToken;
	if(accessToken === null)
		return;

	storage.setItem(accessTokenStorageKey, accessToken);
	return Object.assign({}, state, {
		isAuthorized: true
	});
};

handlers[ActionTypes.SIGNOUT] = (state, action) => {
	storage.removeItem(accessTokenStorageKey);
	return Object.assign({}, state, {
		isAuthorized: false
	});
};

handlers[ActionTypes.RESET] = () => initialState;

export default createReducer(initialState, handlers);