import i18n from "i18next";

import * as ActionTypes from "./commonActionTypes";

export const changeLanguageRequest = lang => {
	return dispatch => {
		i18n.changeLanguage(lang, (err, t) => {
			if (err && window.Raven)
				window.Raven.captureException(err);
		});
	};
};

export const setCurrentLanguage = lang => {
	return dispatch => {
		dispatch({
			type: ActionTypes.SET_CURRENT_LANGUAGE,
			lang: lang
		});
	};
};