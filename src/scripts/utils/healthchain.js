const AUTH_GATE_URL = "http://localhost:7000/authorization-gate";
const SUCCESS_URL = `${window.location.origin}/auth-success`;
const FAILURE_URL = `${window.location.origin}/`;

export const HCLogin = () => {
	const url = `${AUTH_GATE_URL}?success=${encodeURIComponent(SUCCESS_URL)}&failure=${encodeURIComponent(FAILURE_URL)}`;
	window.location.href = url;
};