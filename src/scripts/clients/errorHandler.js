import { ravenConfigKey, constants } from "config";

const useRaven = window.Raven !== undefined;
if(useRaven && ravenConfigKey) {
	window.Raven.config(ravenConfigKey, {
		environment: constants.ENVIRONMENT,
		autoBreadcrumbs: {
			"xhr": true,
			"console": constants.ENVIRONMENT == "production",
			"dom": true,
			"location": true
		}
	}).install();
}

const errorHandler = state => {
	const statusCode = state.error.response != null ? state.error.response.status : null;
	if(useRaven && (statusCode == null || statusCode >= 405)) {
		window.Raven.captureException(state.error);
	}
	
	if(state.action.types && state.action.types.length >= 3){
		let errorData = {};
		if(state.error != null && state.error.response != null && state.error.response.data != null)
			errorData = state.error.response.data;
		else if (state.error.message != null)
			errorData.message = state.error.message;
		state.dispatch({
			type: state.action.types[2],
			error: state.error,
			errorData,
			meta: {
				previousAction: state.action
			}
		});
	} 
	
};

export default errorHandler;