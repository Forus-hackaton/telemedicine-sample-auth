import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { I18nextProvider } from "react-i18next";

import Router from "router";
import store, { history } from "store";
import i18n from "i18n";
import { accessTokenStorageKey } from "config";
import { restoreAuth } from "auth/authActions";

if("ontouchstart" in window || navigator.maxTouchPoints) document.body.classList.add("_touch");
// Restore auth from storage
store.dispatch(restoreAuth());
window.addEventListener("storage", event => {
	if(event.key === accessTokenStorageKey)
		store.dispatch(restoreAuth());
});

render(
	<I18nextProvider i18n={i18n}>
		<Provider store={ store }>
			<Router history={ history } />
		</Provider>
	</I18nextProvider>, 
	document.getElementById("root")
);