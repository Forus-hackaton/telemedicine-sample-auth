import React, { Component } from "react";
import {connect} from "react-redux";

import styles from "./Cabinet.less";

const mapStateToProps = state => ({
	profile: state.app.auth.profile
});
const mapDispatchToProps = {};

class Cabinet extends Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
	}

	render = () => {
		const {
			profile = null
		} = this.props;

		if(profile === null) {
			return null;
		}

		return (
			<div className={styles.root}>
				<div className={styles.container}>
					<h1>Medical Card</h1>
					<div className={styles.card}>
						<div className={styles.leftCol}>
							<img className={styles.image} src={profile.image} />
						</div>
						<div className={styles.rightCol}>
							<h2 className={styles.name}>{profile.firstName} {profile.lastName}</h2>
						</div>
					</div>
				</div>
			</div>
		);
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Cabinet);