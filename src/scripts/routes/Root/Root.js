import React, { Component } from "react";
import { connect } from "react-redux";

import styles from "./Root.less";

const mapStateToProps = state => ({});
const mapDispatchToProps = {};

class Root extends Component {
	render = () => {
		return (
			<div>
				<div className={styles.header}>
					<div className={styles.headerContainer}>
						<div className={styles.headerLogo}>Telemedicine Service</div>
						<div className={styles.headerNav}>
							<a className={styles.headerLink}>Cabinet</a>
							<a className={styles.headerLink}>Doctors</a>
							<a className={styles.headerLink}>Pricing</a>
						</div>
					</div>
				</div>
				{this.props.children}
			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Root);