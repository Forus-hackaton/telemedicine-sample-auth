import React, { Component } from "react";
import {connect} from "react-redux";

import { HCLogin } from "utils/healthchain";
import styles from "./PublicHome.less";

const mapStateToProps = state => ({});
const mapDispatchToProps = {};


window.test = () => {
	alert("test");
};
class PublicHome extends Component {

	login = () => {
		HCLogin();
		// .then((data) => {
		// 	alert("Success");
		// }, (error) => {
		// 	alert("Error");
		// });
	}

	render = () => {
		return (
			<div className={styles.root}>
				<div className={styles.banner} style={{ backgroundImage: "url(https://media.istockphoto.com/photos/doctor-and-nurse-discussing-over-digital-tablet-picture-id629774960?s=2048x2048)"}}>
					<div className={styles.bannerContainer}>
						<div className={styles.bannerContent}>
							<h1 className={styles.bannerTitle}>
								This&apos;s example
								<br />
								<small className={styles.bannerTitleSeparator}>of any</small>
								<br />
								Telemedicine service</h1>
							<button className={styles.bannerAction} onClick={this.login}>Login</button>
						</div>
					</div>
				</div>
			</div>
		);
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(PublicHome);