import React, { Component } from "react";
import {connect} from "react-redux";
import { push } from "react-router-redux";
import qs from "qs";

import { setProfile } from "auth/authActions";

const mapStateToProps = state => ({});
const mapDispatchToProps = {
	setProfile,
	push
};

class AuthSuccess extends Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		const params = qs.parse(window.location.search.replace("?", ""));
		let data = null;

		try {
			data = JSON.parse(decodeURIComponent(params.d));
		} catch(error) {} 

		if (data !== null) {
			this.props.setProfile(data);
			this.props.push("/cabinet");
		} else {
			alert("Error");
		}
		
	}

	render = () => {
		return null;
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthSuccess);